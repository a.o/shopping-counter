//use structopt::StructOpt;

//#[derive(StructOpt)]
//truct Cli {
//    pattern: String,
  //  #[structopt(parse(from_os_str))]
   // path: std::path::PathBuf,
//}
pub mod schema;
pub mod models;
pub mod month;

#[macro_use]
extern crate diesel;
extern crate dotenv;

use diesel::prelude::*;
use diesel::sqlite::SqliteConnection;
use dotenv::dotenv;
use std::env;
use self::month::Month;
use self::models::{Product, NewProduct};
use schema::products;
use std::convert::TryInto;

fn establish_connection() -> SqliteConnection {
    dotenv().ok();
    let database_url = env::var("DATABASE_URL")
	.expect("DATABASE_URL must be set");
    SqliteConnection::establish(&database_url)
	.unwrap_or_else(|_| panic!("Error connecting to {}", 
                                   database_url))
}


pub fn insert_product<'a> (conn: &SqliteConnection, name: &'a str, price: &'a f32, whole: &'a i32, date: &'a str, store: Option<&'a str>) {
    
    let new_product = NewProduct {
	name : name,
	price : price,
	whole : whole,
	date : date,
	store : store,
    };

    let result = diesel::insert_into(products::table)
	.values(&new_product)
	.execute(conn)
	.expect("Error saving product");
    println!("{}", result);
}

fn get_by_name<'a>(p_name: &'a str) -> Vec<Product> {
    use schema::products::dsl::*;
    
    let connection = establish_connection();
    products.filter(name.eq(p_name))
	.load::<Product>(&connection)
	.expect("Error loading product")
}
fn total(products : Vec<Product>) -> f32 {
    products.iter().fold(0.0, |acc, p| acc + p.price)
}

fn avg(products : Vec<Product>) -> f32 {
    let n = products.len();
    let total = total(products);
    total / n as f32
}

pub fn get_avg_price<'a>(p_name : &'a str) -> f32 {
    avg(get_by_name(p_name))
}

fn min(products : Vec<Product>) -> f32 {
    products.iter().fold(1000.0, |acc, p|
			 if p.price < acc {p.price}
			 else {acc})   
}

pub fn get_min_price<'a>(p_name : &'a str) -> f32 {
    min(get_by_name(p_name))
}

fn get_by_date<'a>(p_date : &'a str) -> Vec<Product> {
    use schema::products::dsl::*;
    let connection = establish_connection();
    products.filter(date.eq(p_date))
	.load::<Product>(&connection)
	.expect("Error loading product: ")
}

pub fn get_price_on_date<'a>(date : &'a str) -> f32 {
    total(get_by_date(date))
}

fn get_by_date_interval<'a>(date_f : &'a str, date_t : &'a str) -> Vec<Product> {
    use schema::products::dsl::*;
    let connection = establish_connection();
    products.filter(date.gt(date_f).and(date.lt(date_t)))
	.load::<Product>(&connection)
	.expect("Error loading product: ")
}

pub fn get_price_on_date_interval<'a>(date_f : &'a str, date_t : &'a str) -> f32 {
    total(get_by_date_interval(date_f, date_t))
}
//optional year, if no year given take from current year
fn get_by_month(month : i32, year : i32) -> Vec<Product> {
    let date_f = format!("01/{}/{}", month, year);
    let date_t = format!("31/{}/{}", month, year);
    get_by_date_interval(&date_f, &date_t)
}

pub fn get_price_by_month(month : i32, year : i32) -> f32 {
    total(get_by_month(month, year))
}

fn main() {
    //  Cli::from_args();
    let _connection = establish_connection();
  // insert_product(&connection, "cucumber", &0.57, &1, "2020-06-05", Some("Tesco"));
    println!("{:?}", Month::December);
}

#[cfg(tests)]
mod tests {

    use super::*;
    
    #[test]
    fn test_avg() {
	let p1 = Product {
	    id : 0,
	    name : "test1",
	    price : 0.15,
	    whole : 1,
	    date : "test",
	    store : "store",
	};
	let p2 = Product {
	    id:1,
	    name : "test2",
	    price : 0.45,
	    whole : 1,
	    date : "test",
	    store : "store",
	};
	let p3 = Product {
	    id : 2,
	    name : "test3",
	    price : 1.2,
	    whole : 1,
	    date : "test",
	    store : "store",
	};
	let tester = vec![p1, p2, p3];
	assert_eq!(avg(tester), 0.6);
    }
}
