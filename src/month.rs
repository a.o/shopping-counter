#[derive(Copy, Clone, Debug, PartialEq, PartialOrd)]
pub enum Month {
    January,
    February,
    March,
    April,
    May,
    June,
    July,
    August,
    September,
    October,
    November,
    December,
}

impl Month {
    
    fn number(self) -> i32 {
	match self {
	    Month::January => 1,
	    Month::February => 2,
	    Month::March => 3,
	    Month::April => 4,
	    Month::May => 5,
	    Month::June => 6,
	    Month::July => 7,
	    Month::August => 8,
	    Month::September => 9,
	    Month::October => 10,
	    Month::November => 11,
	    Month::December => 12,
	}
    }
}

use std::convert::TryFrom;

impl TryFrom<i32> for Month {
    type Error = ();

    fn try_from(value : i32) -> Result<Self, Self::Error> {
	match value {
	    1 => Ok(Month::January),
	    2 => Ok(Month::February),
	    3 => Ok(Month::March),
	    4 => Ok(Month::April),
	    5 => Ok(Month::May),
	    6 => Ok(Month::June),
	    7 => Ok(Month::July),
	    8 => Ok(Month::August),
	    9 => Ok(Month::September),
	    10 => Ok(Month::October),
	    11 => Ok(Month::November),
	    12 => Ok(Month::December),
	    _ => Err(()),
	}
    }
}
