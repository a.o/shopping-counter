#[derive(Queryable)]
pub struct Product {
    pub id: i32,
    pub name: String,
    pub price: f32,
    pub whole: i32,
    pub date: String,
    pub store: Option<String>,
}

use super::schema::products;

#[derive(Insertable)]
#[table_name="products"]
pub struct NewProduct<'a> {
    pub name: &'a str,
    pub price: &'a f32,
    pub whole: &'a i32,
    pub date: &'a str,
    pub store: Option<&'a str>,
}
