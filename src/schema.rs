table! {
    products (id) {
        id -> Integer,
        name -> Text,
        price -> Float,
        whole -> Integer,
        date -> Text,
        store -> Nullable<Text>,
    }
}
