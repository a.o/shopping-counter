-- Your SQL goes here
CREATE TABLE products (
        id INTEGER NOT NULL PRIMARY KEY,
        name TEXT NOT NULL,
        price REAL NOT NULL,
        whole INTEGER NOT NULL,
        date TEXT NOT NULL,
        store TEXT)

